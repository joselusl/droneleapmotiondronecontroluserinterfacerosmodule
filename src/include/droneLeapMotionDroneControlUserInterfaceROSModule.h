//////////////////////////////////////////////////////
//  droneLeapMotionDroneControlUserInterfaceROSModule.h
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Oct 27, 2013
//      Author: joselusl
//
//////////////////////////////////////////////////////

#ifndef DRONE_LEAP_MOTION_ROS_MODULE_H
#define DRONE_LEAP_MOTION_ROS_MODULE_H




//I/O stream
//std::cout
#include <iostream>

//Vector
//std::vector
#include <vector>

//String
//std::string, std::getline()
#include <string>

//String stream
//std::istringstream
#include <sstream>

//File Stream
//std::ofstream, std::ifstream
#include <fstream>


// OpenCV
#include "opencv2/opencv.hpp"


#include "pugixml.hpp"


// ROS
#include "ros/ros.h"



//Drone module
#include "droneModuleROS.h"


#include "leap_motion/leapros.h"


#include "communication_definition.h"


#include "referenceFrames.h"


#include "droneMsgsROS/dronePitchRollCmd.h"
#include "droneMsgsROS/droneDYawCmd.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"





class leap_motion_movement_configuration
{
public:
    bool enabled;

    double eq_point;
    double dead_zone_max;
    double dead_zone_min;
    double saturation_max;
    double saturation_min;

public:
    leap_motion_movement_configuration();

};


/////////////////////////////////////////
// Class DroneLeapMotionInterfaceROSModule
//
//   Description
//
/////////////////////////////////////////
class DroneLeapMotionInterfaceROSModule : public DroneModule
{	
protected:
    std::string configFile;
    int readConfigFile(std::string configFile);


protected:
    leap_motion_movement_configuration pitch_roll_config;
    leap_motion_movement_configuration dyaw_config;
    leap_motion_movement_configuration daltitude_config;


public:
    DroneLeapMotionInterfaceROSModule();
    ~DroneLeapMotionInterfaceROSModule();
	
public:
    virtual void open(ros::NodeHandle & nIn, std::string moduleName);
	void close();

protected:
    void readParameters();

protected:
    bool init();

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run -> Not needed. Async
public:
    bool run();



    // Subscriber to leap motion
protected:
    std::string leapMotionTopicName;
    ros::Subscriber leapMotionSub;
    void leapMotionCallback(const leap_motion::leapros::ConstPtr& msg);
protected:
    leap_motion::leapros leapMotionMsgOld;
    bool flagHandsLost;


    // Publisher of drone commands
protected:
    std::string dronePitchRollCmdTopicName;
    ros::Publisher dronePitchRollCommandsPub;
    droneMsgsROS::dronePitchRollCmd dronePitchRollCmdMsg;

    std::string droneDYawCmdTopicName;
    ros::Publisher droneDYawCmdPub;
    droneMsgsROS::droneDYawCmd droneDYawCmdMsg;


    std::string droneDAltitudeCmdTopicName;
    ros::Publisher droneDAltitudeCmdPub;
    droneMsgsROS::droneDAltitudeCmd droneDAltitudeCmdMsg;



    // Function for the commands
protected:
    double functionWithSaturationAndDeadZone(double measure, double eqPoint, double maxVal, double minVal, double maxDeadInc, double minDeadInc);

};






#endif
