//////////////////////////////////////////////////////
//  droneLeapMotionDroneControlUserInterfaceROSModule.cpp
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Oct 27, 2013
//      Author: joselusl
//
//////////////////////////////////////////////////////


#include "droneLeapMotionDroneControlUserInterfaceROSModule.h"



using namespace std;




leap_motion_movement_configuration::leap_motion_movement_configuration()
{
    enabled=false;

    eq_point=0.0;
    dead_zone_max=0.0;
    dead_zone_min=0.0;
    saturation_max=0.0;
    saturation_min=0.0;


    return;
}






DroneLeapMotionInterfaceROSModule::DroneLeapMotionInterfaceROSModule() : DroneModule(droneModule::active,1.0)
{
    flagHandsLost=true;

    return;
}


DroneLeapMotionInterfaceROSModule::~DroneLeapMotionInterfaceROSModule()
{
	close();
	return;
}


bool DroneLeapMotionInterfaceROSModule::init()
{
    DroneModule::init();



    //end
    return true;
}


int DroneLeapMotionInterfaceROSModule::readConfigFile(std::string configFile)
{

    //XML document
    pugi::xml_document doc;
    std::ifstream nameFile(configFile.c_str());
    pugi::xml_parse_result result = doc.load(nameFile);

    if(!result)
    {
        cout<<"I cannot open xml"<<endl;
        return 0;
    }

    std::string readingValue;
    pugi::xml_node leap_motion_user_interface = doc.child("leap_motion_user_interface");


    // Configurations

    // Pitch Roll
    {
    readingValue=leap_motion_user_interface.child("pitch_roll").child_value("enabled");
    istringstream convert_enabled(readingValue);
    convert_enabled>>this->pitch_roll_config.enabled;

    readingValue=leap_motion_user_interface.child("pitch_roll").child("configuration").child_value("eq_point");
    istringstream convert_eq_point(readingValue);
    convert_eq_point>>this->pitch_roll_config.eq_point;

    readingValue=leap_motion_user_interface.child("pitch_roll").child("configuration").child("dead_zone").child_value("max_increment");
    istringstream convert_deadzone_max(readingValue);
    convert_deadzone_max>>this->pitch_roll_config.dead_zone_max;

    readingValue=leap_motion_user_interface.child("pitch_roll").child("configuration").child("dead_zone").child_value("min_increment");
    istringstream convert_deadzone_min(readingValue);
    convert_deadzone_min>>this->pitch_roll_config.dead_zone_min;

    readingValue=leap_motion_user_interface.child("pitch_roll").child("configuration").child("saturation").child_value("max_increment");
    istringstream convert_saturation_max(readingValue);
    convert_saturation_max>>this->pitch_roll_config.saturation_max;

    readingValue=leap_motion_user_interface.child("pitch_roll").child("configuration").child("saturation").child_value("min_increment");
    istringstream convert_saturation_min(readingValue);
    convert_saturation_min>>this->pitch_roll_config.saturation_min;
    }


    // Dyaw
    {
    readingValue=leap_motion_user_interface.child("dyaw").child_value("enabled");
    istringstream convert_enabled(readingValue);
    convert_enabled>>this->dyaw_config.enabled;

    readingValue=leap_motion_user_interface.child("dyaw").child("configuration").child_value("eq_point");
    istringstream convert_eq_point(readingValue);
    convert_eq_point>>this->dyaw_config.eq_point;

    readingValue=leap_motion_user_interface.child("dyaw").child("configuration").child("dead_zone").child_value("max_increment");
    istringstream convert_deadzone_max(readingValue);
    convert_deadzone_max>>this->dyaw_config.dead_zone_max;

    readingValue=leap_motion_user_interface.child("dyaw").child("configuration").child("dead_zone").child_value("min_increment");
    istringstream convert_deadzone_min(readingValue);
    convert_deadzone_min>>this->dyaw_config.dead_zone_min;

    readingValue=leap_motion_user_interface.child("dyaw").child("configuration").child("saturation").child_value("max_increment");
    istringstream convert_saturation_max(readingValue);
    convert_saturation_max>>this->dyaw_config.saturation_max;

    readingValue=leap_motion_user_interface.child("dyaw").child("configuration").child("saturation").child_value("min_increment");
    istringstream convert_saturation_min(readingValue);
    convert_saturation_min>>this->dyaw_config.saturation_min;
    }


    // Daltitude
    {
    readingValue=leap_motion_user_interface.child("daltitude").child_value("enabled");
    istringstream convert_enabled(readingValue);
    convert_enabled>>this->daltitude_config.enabled;

    readingValue=leap_motion_user_interface.child("daltitude").child("configuration").child_value("eq_point");
    istringstream convert_eq_point(readingValue);
    convert_eq_point>>this->daltitude_config.eq_point;

    readingValue=leap_motion_user_interface.child("daltitude").child("configuration").child("dead_zone").child_value("max_increment");
    istringstream convert_deadzone_max(readingValue);
    convert_deadzone_max>>this->daltitude_config.dead_zone_max;

    readingValue=leap_motion_user_interface.child("daltitude").child("configuration").child("dead_zone").child_value("min_increment");
    istringstream convert_deadzone_min(readingValue);
    convert_deadzone_min>>this->daltitude_config.dead_zone_min;

    readingValue=leap_motion_user_interface.child("daltitude").child("configuration").child("saturation").child_value("max_increment");
    istringstream convert_saturation_max(readingValue);
    convert_saturation_max>>this->daltitude_config.saturation_max;

    readingValue=leap_motion_user_interface.child("daltitude").child("configuration").child("saturation").child_value("min_increment");
    istringstream convert_saturation_min(readingValue);
    convert_saturation_min>>this->daltitude_config.saturation_min;
    }




    // Display configurations
    std::cout<<"Configurations:"<<std::endl;
    std::cout<<"+pitch_roll:"<<std::endl;
    std::cout<<" +enabled="<<this->pitch_roll_config.enabled<<std::endl;
    std::cout<<" +eq_point (deg)="<<this->pitch_roll_config.eq_point<<std::endl;
    std::cout<<" +dead_zone_max (deg)="<<this->pitch_roll_config.dead_zone_max<<std::endl;
    std::cout<<" +dead_zone_min (deg)="<<this->pitch_roll_config.dead_zone_min<<std::endl;
    std::cout<<" +saturation_max (deg)="<<this->pitch_roll_config.saturation_max<<std::endl;
    std::cout<<" +saturation_min (deg)="<<this->pitch_roll_config.saturation_min<<std::endl;

    std::cout<<"+dyaw:"<<std::endl;
    std::cout<<" +enabled="<<this->dyaw_config.enabled<<std::endl;
    std::cout<<" +eq_point (deg)="<<this->dyaw_config.eq_point<<std::endl;
    std::cout<<" +dead_zone_max (deg)="<<this->dyaw_config.dead_zone_max<<std::endl;
    std::cout<<" +dead_zone_min (deg)="<<this->dyaw_config.dead_zone_min<<std::endl;
    std::cout<<" +saturation_max (deg)="<<this->dyaw_config.saturation_max<<std::endl;
    std::cout<<" +saturation_min (deg)="<<this->dyaw_config.saturation_min<<std::endl;

    std::cout<<"+daltitude:"<<std::endl;
    std::cout<<" +enabled="<<this->daltitude_config.enabled<<std::endl;
    std::cout<<" +eq_point (mm)="<<this->daltitude_config.eq_point<<std::endl;
    std::cout<<" +dead_zone_max (mm)="<<this->daltitude_config.dead_zone_max<<std::endl;
    std::cout<<" +dead_zone_min (mm)="<<this->daltitude_config.dead_zone_min<<std::endl;
    std::cout<<" +saturation_max (mm)="<<this->daltitude_config.saturation_max<<std::endl;
    std::cout<<" +saturation_min (mm)="<<this->daltitude_config.saturation_min<<std::endl;


    return 1;
}


void DroneLeapMotionInterfaceROSModule::readParameters()
{
    std::cout<<"Parameters:"<<std::endl;

    // Config file
    ros::param::param<std::string>("config_file", configFile, "leapMotionUserInterface.xml");
    std::cout<<"+config_file="<<configFile<<std::endl;


    // Topics
    ros::param::param<std::string>("leap_motion_topic_name", leapMotionTopicName, "leapmotion/data");
    std::cout<<"+leap_motion_topic_name="<<leapMotionTopicName<<std::endl;

    ros::param::param<std::string>("drone_pitch_roll_cmd_topic_name", dronePitchRollCmdTopicName, "command/pitch_roll");
    std::cout<<"+drone_pitch_roll_cmd_topic_name="<<dronePitchRollCmdTopicName<<std::endl;

    ros::param::param<std::string>("drone_dyaw_cmd_topic_name", droneDYawCmdTopicName, "command/dYaw");
    std::cout<<"+drone_dyaw_cmd_topic_name="<<droneDYawCmdTopicName<<std::endl;

    ros::param::param<std::string>("drone_daltitude_cmd_topic_name", droneDAltitudeCmdTopicName, "command/dAltitude");
    std::cout<<"+drone_daltitude_cmd_topic_name="<<droneDAltitudeCmdTopicName<<std::endl;


    return;
}


void DroneLeapMotionInterfaceROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
	//Node
    DroneModule::open(nIn,moduleName);

    //Init
    if(!init())
        cout<<"Error init"<<endl;


    // Read parameters
    readParameters();


    // Read Config File
    std::string configFileComplete=stackPath+"configs/drone"+stringId+"/"+configFile;
    readConfigFile(configFileComplete);
	

    // Topics
    // Subscribers
    leapMotionSub=n.subscribe(leapMotionTopicName, 1, &DroneLeapMotionInterfaceROSModule::leapMotionCallback, this);

    // Publishers
    dronePitchRollCommandsPub=n.advertise<droneMsgsROS::dronePitchRollCmd>(dronePitchRollCmdTopicName, 1, true);

    droneDYawCmdPub=n.advertise<droneMsgsROS::droneDYawCmd>(droneDYawCmdTopicName,1,true);

    droneDAltitudeCmdPub=n.advertise<droneMsgsROS::droneDAltitudeCmd>(droneDAltitudeCmdTopicName,1,true);


    // Open
    droneModuleOpened=true;

    // Start
    //moduleStarted=true;

	//End
	return;
}


void DroneLeapMotionInterfaceROSModule::close()
{
    DroneModule::close();



    return;
}


bool DroneLeapMotionInterfaceROSModule::resetValues()
{


    return true;
}


bool DroneLeapMotionInterfaceROSModule::startVal()
{


    //End
    return DroneModule::startVal();
}


bool DroneLeapMotionInterfaceROSModule::stopVal()
{


    return DroneModule::stopVal();
}


bool DroneLeapMotionInterfaceROSModule::run()
{
    if(!DroneModule::run())
        return false;

    if(droneModuleOpened==false)
        return false;



    return true;
}



void DroneLeapMotionInterfaceROSModule::leapMotionCallback(const leap_motion::leapros::ConstPtr& msg)
{
    if(!droneModuleOpened || !moduleStarted)
        return;


    // Check the message
    if(msg->direction.x==leapMotionMsgOld.direction.x && msg->direction.y==leapMotionMsgOld.direction.y && msg->direction.z==leapMotionMsgOld.direction.z &&
            msg->normal.x==leapMotionMsgOld.normal.x && msg->normal.y==leapMotionMsgOld.normal.y && msg->normal.z==leapMotionMsgOld.normal.z &&
            msg->palmpos.x==leapMotionMsgOld.palmpos.x && msg->palmpos.y==leapMotionMsgOld.palmpos.y && msg->palmpos.z==leapMotionMsgOld.palmpos.z)
    {
        // Check if hands were lost
        if(flagHandsLost)
        {
            return;
        }
        else
        {
            flagHandsLost=true;

            // Publish empty command
            // Pitch roll
            dronePitchRollCmdMsg.header.stamp=ros::Time::now();
            dronePitchRollCmdMsg.pitchCmd=0.0;
            dronePitchRollCmdMsg.rollCmd=0.0;

            dronePitchRollCommandsPub.publish(dronePitchRollCmdMsg);

            // dYaw
            droneDYawCmdMsg.header.stamp=ros::Time::now();
            droneDYawCmdMsg.dYawCmd=0.0;

            droneDYawCmdPub.publish(droneDYawCmdMsg);

            // dAltitude
            droneDAltitudeCmdMsg.header.stamp=ros::Time::now();

            // msg->palmpos (en mm)
            droneDAltitudeCmdMsg.header.stamp=ros::Time::now();
            droneDAltitudeCmdMsg.dAltitudeCmd=0.0;

            droneDAltitudeCmdPub.publish(droneDAltitudeCmdMsg);



            return;
        }
    }
    else
    {
        leapMotionMsgOld=*msg;
        flagHandsLost=false;
    }



    cv::Mat handRefFrameWrtLeapMotionRefFrame=cv::Mat::zeros(4,4,CV_64F);


    handRefFrameWrtLeapMotionRefFrame.at<double>(0,0)=msg->direction.x;
    handRefFrameWrtLeapMotionRefFrame.at<double>(0,1)=msg->direction.y;
    handRefFrameWrtLeapMotionRefFrame.at<double>(0,2)=msg->direction.z;

    handRefFrameWrtLeapMotionRefFrame.at<double>(1,0)=-msg->direction.y*msg->normal.z + msg->direction.z*msg->normal.y;
    handRefFrameWrtLeapMotionRefFrame.at<double>(1,1)=-msg->direction.z*msg->normal.x + msg->direction.x*msg->normal.z;
    handRefFrameWrtLeapMotionRefFrame.at<double>(1,2)=-msg->direction.x*msg->normal.y + msg->direction.y*msg->normal.x;

    handRefFrameWrtLeapMotionRefFrame.at<double>(2,0)=msg->normal.x;
    handRefFrameWrtLeapMotionRefFrame.at<double>(2,1)=msg->normal.y;
    handRefFrameWrtLeapMotionRefFrame.at<double>(2,2)=msg->normal.z;


    handRefFrameWrtLeapMotionRefFrame.at<double>(3,3)=1.0;

//    std::cout<<"handRefFrameWrtLeapMotionRefFrame"<<std::endl;
//    std::cout<<handRefFrameWrtLeapMotionRefFrame<<std::endl;



    cv::Mat transLeapMotioRefFrame2DroneRefFrame=cv::Mat::zeros(4,4,CV_64F);

    transLeapMotioRefFrame2DroneRefFrame.at<double>(0,0)=1;
    transLeapMotioRefFrame2DroneRefFrame.at<double>(1,2)=-1;
    transLeapMotioRefFrame2DroneRefFrame.at<double>(2,1)=1;

    transLeapMotioRefFrame2DroneRefFrame.at<double>(3,3)=1;


    cv::Mat handRefFrameWrtDroneRefFrame=cv::Mat::zeros(4,4,CV_64F);
    handRefFrameWrtDroneRefFrame=handRefFrameWrtLeapMotionRefFrame*transLeapMotioRefFrame2DroneRefFrame;
    handRefFrameWrtDroneRefFrame=handRefFrameWrtDroneRefFrame.inv();



//    std::cout<<"transLeapMotioRefFrame2DroneRefFrame"<<std::endl;
//    std::cout<<transLeapMotioRefFrame2DroneRefFrame<<std::endl;

//    std::cout<<"handRefFrameWrtDroneRefFrame"<<std::endl;
//    std::cout<<handRefFrameWrtDroneRefFrame<<std::endl;



    cv::Mat homogMat=cv::Mat::zeros(4,4,CV_32F);
    for(unsigned int i=0;i<4;i++)
        for(unsigned int j=0;j<4;j++)
            homogMat.at<float>(i,j)=handRefFrameWrtDroneRefFrame.at<double>(i,j);

//    std::cout<<"homogMat"<<std::endl;
//    std::cout<<homogMat<<std::endl;



    double x,y,z;
    double yaw, pitch, roll;

    referenceFrames::getxyzYPRfromHomogMatrix_wYvPuR(homogMat, &x, &y, &z, &yaw, &pitch, &roll);


    //std::cout<<"yaw="<<yaw<<"; pitch="<<pitch<<"; roll="<<roll<<std::endl;


    //// PITCH-ROLL
    if(pitch_roll_config.enabled)
    {
        dronePitchRollCmdMsg.header.stamp=ros::Time::now();


        double pitchCmd=functionWithSaturationAndDeadZone(pitch*180.0/M_PI, pitch_roll_config.eq_point,
                                                          pitch_roll_config.saturation_max, pitch_roll_config.saturation_min,
                                                          pitch_roll_config.dead_zone_max, pitch_roll_config.dead_zone_min);

        double rollCmd=functionWithSaturationAndDeadZone(roll*180.0/M_PI, pitch_roll_config.eq_point,
                                                         pitch_roll_config.saturation_max, pitch_roll_config.saturation_min,
                                                         pitch_roll_config.dead_zone_max, pitch_roll_config.dead_zone_min);


        dronePitchRollCmdMsg.pitchCmd=pitchCmd;
        dronePitchRollCmdMsg.rollCmd=rollCmd;

        dronePitchRollCommandsPub.publish(dronePitchRollCmdMsg);
    }


    ////// DYAW
    if(dyaw_config.enabled)
    {
        droneDYawCmdMsg.header.stamp=ros::Time::now();

        double DYawCmd=functionWithSaturationAndDeadZone(yaw*180.0/M_PI, dyaw_config.eq_point,
                                                         dyaw_config.saturation_max, dyaw_config.saturation_min,
                                                         dyaw_config.dead_zone_max, dyaw_config.dead_zone_min);

        droneDYawCmdMsg.dYawCmd=DYawCmd;

        droneDYawCmdPub.publish(droneDYawCmdMsg);
    }



    ///// DALTITUDE
    if(daltitude_config.enabled)
    {
        droneDAltitudeCmdMsg.header.stamp=ros::Time::now();

        // msg->palmpos (en mm)
        double DAltitudeCmd=functionWithSaturationAndDeadZone(msg->palmpos.y, daltitude_config.eq_point,
                                                              daltitude_config.saturation_max, daltitude_config.saturation_min,
                                                              daltitude_config.dead_zone_max, daltitude_config.dead_zone_min);

        droneDAltitudeCmdMsg.dAltitudeCmd=DAltitudeCmd;

        droneDAltitudeCmdPub.publish(droneDAltitudeCmdMsg);
    }


    return;
}





double DroneLeapMotionInterfaceROSModule::functionWithSaturationAndDeadZone(double measure, double eqPoint, double maxValInc, double minValInc, double maxDeadInc, double minDeadInc)
{
    double output=0;

    double maxDead=eqPoint+maxDeadInc;
    double minDead=eqPoint+minDeadInc;
    double maxVal=eqPoint+maxValInc;
    double minVal=eqPoint+minValInc;

    // Dead
    if(measure<maxDead && measure>minDead)
    {
        output=0;
        return output;
    }

    // Saturation
    if(measure>maxVal)
    {
        output=1.0;
        return output;
    }

    if(measure<minVal)
    {
        output=-1.0;
        return output;
    }

    // Linear zone
    if(measure>maxDead && measure<maxVal)
    {
        double m=1.0/(maxVal-maxDead);
        output=m*(measure-maxDead);
        return output;
    }

    if(measure<minDead && measure>minVal)
    {
        double m=-1.0/(minVal-minDead);
        output=m*(measure-minDead);
        return output;
    }


    return output;
}
